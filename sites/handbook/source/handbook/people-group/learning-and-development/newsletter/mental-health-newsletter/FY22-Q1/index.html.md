---
layout: handbook-page-toc
title: FY22-Q1 L&D Mental Health Newsletter
---

Thanks for tuning in to the 2nd edition of the GitLab Mental Health newsletter! If you're interested in learning more about the intentions of this newsletter, check out our [goals](/handbook/people-group/learning-and-development/newsletter/mental-health-newsletter/#long-term-goals) listed in the handbook.

## GitLab Resource Feature

Recently, the GitLab team member [modern health benefit](/handbook/total-rewards/benefits/modern-health/) was updated to include [therapy sessions](/handbook/total-rewards/benefits/modern-health/#care). These sessions were added in addition to existing coaching sessions. 

You can learn more about what Modern Health is all about in this [short interview with their CEO and founder](https://youtu.be/-oUb3RcQB3M), or sign up directly in the [Modern Health web platform or mobile app](https://www.joinmodernhealth.com/).

<iframe width="560" height="315" src="https://www.youtube.com/embed/-oUb3RcQB3M" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

If you have questions about how this benefit works, please reach out to the [total rewards team in Slack](https://app.slack.com/client/T02592416/CTVK60M32)


## Taking time for mental health

During the month of March, the Learning and Development team encouraged team members to check out the [Building a rest ethic handbook resources](https://about.gitlab.com/company/culture/all-remote/mental-health/). Included here is:

1. A course by John Fitch called Time Off: Design your Rest Ethic
1. LinkedIn Learning courses on Mindful Stress Management and How to create a life of meaning and purpose
1. A call to action to contribute to our handbook section entitled [Your rest ethic is as important as your work ethic](/company/culture/all-remote/mental-health/#your-rest-ethic-is-as-important-as-your-work-ethic)

![rest-ethic-handbook](/handbook/people-group/learning-and-development/newsletter/mental-health-newsletter/FY22-Q1/rest-ethic.png)

Take some time this month to learn strategies about how you can build your own rest ethic. Remember that team members can use the [Growth and Development benefit](/handbook/total-rewards/benefits/general-and-entity-benefits/#growth-and-development-benefit) to expense courses like the Time Off: Design your Rest Ethic course linked above.

And, keep an eye out for upcoming opportunities to connect with John Fitch and continute the discussion about building a rest ethic for yourself and your team.


## Manager resources

1. We've revamped the [Leadership Chats](/handbook/people-group/learning-and-development/manager-challenge/leadership-chats/) at GitLab to be a safe space for managers to connect and talk about challenging things at work. If you're looking for a space to connect and share experiences with managers across the organization, consider joining one of the sessions hosted each month.
1. Remember that taking [time off](https://about.gitlab.com/handbook/paid-time-off/) is critical to your own success. Take a moment now to schedule time off for the next quarter.
1. The next time that you schedule [time off](https://about.gitlab.com/handbook/paid-time-off/) for yourself, consider sharing with your team what you did to refresh and reset during your time away from work.
1. Read this article shared in the [#mental_health_aware Slack channel this past month](https://app.slack.com/client/T02592416/C834CM4HW) about [being realistic about the time you have](https://hbr.org/2021/03/be-more-realistic-about-the-time-you-have)

## Team member resources and reminders

1. Take a moment to schedule some [time off](https://about.gitlab.com/handbook/paid-time-off/) in the coming weeks to help you reset and rest as we gear up for Q2
1. The [Calm app](https://www.calm.com/) has a fantastic [YouTube channel](https://www.youtube.com/c/calm/featured) with meditations, stories, and discussions about mindfulness practices
1. Here's a great [lo-fi music playlist to turn on in the background while you're working today](https://youtu.be/n_wbIhJIQTU)
1. Join the L&D team in the [#walk-and-learn Slack channel](/handbook/people-group/learning-and-development/linkedin-learning/#walk-and-learn) to practice the April [Skill of the Month](/handbook/people-group/learning-and-development/learning-initiatives/#skill-of-the-month), Managing Stress.

If you can, take a break to [do nothing with Lebron James for 30 seconds](https://youtu.be/LwgnzCLy8Mk)

<iframe width="560" height="315" src="https://www.youtube.com/embed/LwgnzCLy8Mk" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
 
## Discussion 

If you would like to discuss items related to this newsletter, please see the related [issue](https://gitlab.com/gitlab-com/people-group/learning-development/mental-health/-/issues/2). 
